let data = {"type":"FeatureCollection",
"name":"data",
"crs":{"type":"name","properties":{"name":"urn:ogc:def:crs:OGC:1.3:CRS84"}},
"features":
[
    {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                -75.698874,
                4.815264
            ]
        },
        "properties":{
            "id": 1,
            "nombre": "Artesanias LTDA",
            "direccion": "Cra. 7 #24-42",
            "descripcion": "Especialistas en venta de artesanias.",
            "contacto": "3000457815",
            "correo": "serviciocliente@artesanias.com",
            "departamento": "Risaralda",
            "ciudad": "Pereira",
            "tipo": "Artesanias",
            "inventario":[
                {
                    "_id": "61b80a534e38f53ebf25c994",
                    "id_comercio": 1,
                    "id_producto": "p-0002",
                    "referencia": "ref-00002",
                    "marca": "Propia",
                    "nombre": "Bolso Carriel Correa Wayuu Mujer",
                    "unidades": 2,
                    "keywords": [
                        "Bolso",
                        "Carriel",
                        "Wayu",
                        "Mujer"
                    ],
                    "tags": [
                        "Artesanias"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b80af8e248e17bd4637ba2",
                    "id_comercio": 1,
                    "id_producto": "p-0003",
                    "referencia": "ref-00003",
                    "marca": "Artesanias de Colombia",
                    "nombre": "Hamaca Colombiana Doble",
                    "unidades": 1,
                    "keywords": [
                        "Hamaca",
                        "Sombrero",
                        "Ala"
                    ],
                    "tags": [
                        "Artesanias"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b80ba4077df4102e01ce6a",
                    "id_comercio": 1,
                    "id_producto": "p-0001",
                    "referencia": "ref-00001",
                    "marca": "Artesanias de Colombia",
                    "nombre": "Casuela de barro con canasta",
                    "unidades": 3,
                    "keywords": [
                        "Casuela",
                        "Barro",
                        "Taza",
                        "Plato"
                    ],
                    "tags": [
                        "Artesanias"
                    ],
                    "url_imagen": "",
                    "__v": 0
                }
            ]
            
        }
    },
    {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                -75.70425,
                4.808971
            ]
        },
        "properties":{
            "id": 2,
            "nombre": "Electro ferretria del valle",
            "direccion": "Calle. 7 #24-42",
            "descripcion": "Especialistas accesorios de ferreteria",
            "contacto": "3000457815",
            "correo": "serviciocliente@ferreteria.com",
            "departamento": "Risaralda",
            "ciudad": "Pereira",
            "tipo": "Ferreteria",
            "inventario":[
                {
                    "_id": "61b80cc308da8b2065924f98",
                    "id_comercio": 2,
                    "id_producto": "p-0004",
                    "referencia": "ref-00004",
                    "marca": "Stanley",
                    "nombre": "Alicates Punta larga y mandíbula Curva con cortador de alambre",
                    "unidades": 3,
                    "keywords": [
                        "Alicate",
                        "Cortador",
                        "Alambre",
                        "Curva"
                    ],
                    "tags": [
                        "Ferretería"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b80d64765ae1cea0755619",
                    "id_comercio": 2,
                    "id_producto": "p-0005",
                    "referencia": "ref-0005",
                    "marca": "Stanley",
                    "nombre": "ALICATE DE PRESIÓN VISEGRIP DE MORDAZA RECTA",
                    "unidades": 5,
                    "keywords": [
                        "Alicate",
                        "Presión",
                        "Mordaza"
                    ],
                    "tags": [
                        "Ferretería"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b80e46b8205bb78a6b43c4",
                    "id_comercio": 2,
                    "id_producto": "p-0006",
                    "referencia": "ref-0006",
                    "marca": "Stanley",
                    "nombre": "Juego de Botadores Stanley",
                    "unidades": 2,
                    "keywords": [
                        "Botadores",
                        "Cinceles",
                        "Punzones"
                    ],
                    "tags": [
                        "Ferretería"
                    ],
                    "url_imagen": "",
                    "__v": 0
                }
            ]
            
        }
    },
    {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                -75.690735,
                4.814663
            ]
        },
        "properties":{
            "id": 3,
            "nombre": "Miscelanea Todo En Uno",
            "direccion": "Cl 15 #6-103 a 6-1",
            "descripcion": "Aquíe encuentras todo lo que necesitas",
            "contacto": "3125412184",
            "correo": "serviciocliente@miscelanea.com",
            "departamento": "Risaralda",
            "ciudad": "Pereira",
            "tipo": "Miscelanea",
            "inventario":[
                {
                    "_id": "61b80f1abdb9914e43bfd694",
                    "id_comercio": 3,
                    "id_producto": "p-0007",
                    "referencia": "ref-0007",
                    "marca": "Misc",
                    "nombre": "Destapador de cerveza con iman",
                    "unidades": 5,
                    "keywords": [
                        "Destapador",
                        "Iman",
                        "Cerveza"
                    ],
                    "tags": [
                        "Miscelanea"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b80fe3cfbe1f1267011981",
                    "id_comercio": 3,
                    "id_producto": "p-0008",
                    "referencia": "ref-0008",
                    "marca": "MagicTab",
                    "nombre": "Tablero didactico en acrilico",
                    "unidades": 2,
                    "keywords": [
                        "Tablero",
                        "Escolar",
                        "Didactico",
                        "Acrilico",
                        "Acrílico"
                    ],
                    "tags": [
                        "Miscelanea",
                        "Papelería"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b8104737f4e29cf77b2665",
                    "id_comercio": 3,
                    "id_producto": "p-0009",
                    "referencia": "ref-0009",
                    "marca": "ZurTij",
                    "nombre": "Tijeras para surdos",
                    "unidades": 10,
                    "keywords": [
                        "Tijeras",
                        "Zurdos",
                        "Izquierda",
                        "Manos",
                        "Cortar"
                    ],
                    "tags": [
                        "Miscelanea",
                        "Papelería"
                    ],
                    "url_imagen": "",
                    "__v": 0
                }
            ]
            
        }
    },
    {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                -75.708849,
                4.811724
            ]
        },
        "properties":{
            "id": 4,
            "nombre": "Tecnología S.A.S",
            "direccion": "Cl. 36 #13-133 a 13-1",
            "descripcion": "Todo en productos de tecnología",
            "contacto": "31441454811",
            "correo": "serviciocliente@tecnologia.com",
            "departamento": "Risaralda",
            "ciudad": "Pereira",
            "tipo": "Tecnologia",
            "inventario":[
                {
                    "_id": "61b812238c1562b1cb596e3b",
                    "id_comercio": 5,
                    "id_producto": "p-0011",
                    "referencia": "ref-0011",
                    "marca": "Hewlett-Packard",
                    "nombre": "Impresora Multifuncional HP Smart Tank 515",
                    "unidades": 1,
                    "keywords": [
                        "Impresora",
                        "Imprimir",
                        "Smart"
                    ],
                    "tags": [
                        "Tecnología"
                    ],
                    "url_imagen": "",
                    "__v": 0
                },
                {
                    "_id": "61b81287a156ea36fa1eb39d",
                    "id_comercio": 5,
                    "id_producto": "p-0012",
                    "referencia": "ref-0012",
                    "marca": "T-UBS",
                    "nombre": "Tarjeta Adaptador Convertidor Lan Red Rj45 Usb 2.0",
                    "unidades": 10,
                    "keywords": [
                        "Adapatador",
                        "Convertidor",
                        "Tarjeta"
                    ],
                    "tags": [
                        "Tecnología"
                    ],
                    "url_imagen": "",
                    "__v": 0
                }
            ]
            
        }
    }
]
}