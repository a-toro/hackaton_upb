const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const inventarioSchema = new Schema({
    id_comercio:{
        type:Number,
        required: true
    },
    id_producto:{
        type:String,
        required: true,
        unique: true
    },
    referencia:{
        type:String,
        required: true
    },
    marca:{
        type:String,
    },
    nombre:{
        type:String,
        required: true
    },
    unidades:{
        type:Number,
        required: true
    },
    keywords:{
        type:Array,
        required: true
    },
    tags:{
        type:Array,
    },
    url_imagen:{
        type:String
    }
    
});

const Inventario = mongoose.model('inventario', inventarioSchema, "inventario");
module.exports =  Inventario;