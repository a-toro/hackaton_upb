const express = require('express');
const routes = express.Router();

const serviciosInventario = require('./services');

routes.get('/', serviciosInventario.getGeoJson);

module.exports = routes;